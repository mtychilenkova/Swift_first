import Foundation
import Darwin

//1 задание
func  fibonacciNumber(count: Int) -> [Int]{
    
    if count == 0 {
        return []
    }
    
    if count == 1 {
        return [1]
    }
    
    var number1 = 1
    var number2 = 1
    var storyPointsNumber = [number1, number2]
    
    for _ in 0 ..< count - 2{
        let number = number1 + number2
        number1 = number2
        number2 = number
        storyPointsNumber.append(number)
    }
    return storyPointsNumber
}

print(fibonacciNumber(count: 0))
print(fibonacciNumber(count: 1))
print(fibonacciNumber(count: 2))
print(fibonacciNumber(count: 7))

//2 задание

var printArrayFor: () -> Void = {
    
    for item in fibonacciNumber(count: 5){
        print(item)
    }
}

printArrayFor()
