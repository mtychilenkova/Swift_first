import UIKit

// 1 задача
var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

// 2 задача
var milkPrice: Double = 3

// 3 задача
var milkPrice: Double = 4.20

// 4 задача
var milkBottleCount: Int?
var profit: Double = 0.0

milkBottleCount = 20
profit = milkPrice * Double(milkBottleCount!)
print(profit)

/*if let milkBottleCount != nil{
    profit = milkPrice * Double(milkBottleCount!)
}
else{
    print("milkBottleCount == nil")
}

print(profit)*/


//5 задача
var employeesList = [String]()

employeesList+=["Иван", "Марфа", "Андрей", "Петр", "Геннадий"]
print(employeesList)


//6 задача
var isEveryoneWorkHard: Bool = false
var workingHours: Int = 28

if workingHours >= 40{
    isEveryoneWorkHard = true
    print(isEveryoneWorkHard)
}
else{
    print(isEveryoneWorkHard)
}
