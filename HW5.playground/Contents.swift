import UIKit
import Foundation

//1 задание

class Shape {
    func calculateArea()->Double{
       fatalError("not implemented")
    }
    
    func calculatePerimeter()->Double{
        fatalError("not implemented")
    }
}

class Rectangle: Shape{
    private let height: Double
    private let width: Double
    
  init(height: Double, width: Double) {
       
        self.height = height
        self.width = width
    }
    
    override func calculateArea() -> Double {
        let s:Double
        
        s = height*width
        
        return s
    }
    
    override func calculatePerimeter() -> Double {
        let p:Double
        
        p = 2*(height+width)
        
        return p
    }
}

class Circle: Shape{
    private let radius: Double
    
    init(radius: Double){
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        let pi:Double = 3.14
        let s:Double
        
        s = pi*pow(radius, 2)
        
        return s
        
    }
    
    override func calculatePerimeter() -> Double {
        let pi:Double = 3.14
        let l: Double
        
        l = 2*radius*pi
        
        return l
    }
}

class Square: Shape{
    private let side: Double
    
    init(side: Double){
        self.side = side
    }
    
    override func calculateArea() -> Double {
        let s: Double
        
        s = pow(side, 2)
        
        return s
    }
    
    override func calculatePerimeter() -> Double {
        let p: Double
        
        p = 2*side
        
        return p
    }
}

var rectangle = Rectangle(height:24.2, width:13.1)
var square = Square(side: 40)
var circle = Circle(radius: 12)
var shapes:[Shape] = [rectangle, square, circle]

for shape in shapes{
    print(shape.calculatePerimeter())
    print(shape.calculateArea())
}

//2 задание

func findIndexWithGenerics<T: Comparable>(stringOf valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
           if value == valueToFind {
               return index
           }
       }
       return nil
   }
    
let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
let arrayOfInt = [1, 2, 3, 4, 7]
let arrayOfDouble = [1.5, 2.3, 3.9, 4.1, 7.4]

if let fundIndex = findIndexWithGenerics(stringOf: 7, in: arrayOfInt) {
    print("Индекс: \(fundIndex)")
}

if let fundIndex = findIndexWithGenerics(stringOf: "пес", in: arrayOfString) {
    print("Индекс: \(fundIndex)")
}

if let fundIndex = findIndexWithGenerics(stringOf: 7.4, in: arrayOfDouble) {
    print("Индекс: \(fundIndex)")
}
