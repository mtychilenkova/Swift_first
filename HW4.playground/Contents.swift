import UIKit

//1 и 3 задание

class inputField{
    let header: String
    let length: Int
    var code: Int?
    let priority: Priority
    var placeholder: String?
    enum Priority: String{
        case low = "low"
        case middle
        case high
    }
    
    init(header: String, length: Int, priority: Priority){
        self.header = header
        self.length = length
        self.priority = priority
        }
    
    convenience init (header: String, length: Int, code: Int? = nil, priority: Priority, placeholder: String? = nil){
        self.init(header: header, length: length, priority: priority)
        self.code = code
        self.placeholder = placeholder
    }
    
    func lengthMeasure()-> Bool{
        if length < 25{
            return true
        }
        return false
    }
    
    func changePlaceholder(with newPlaceholder: String){
       placeholder = newPlaceholder
    }
}

let nameField = inputField(header: "Name", length: 25, code: 1, priority: .high, placeholder: "Type your name")
let surnameField = inputField(header: "Surname", length: 25, code: 2, priority: .high, placeholder: "Type your surname")
let ageField = inputField(header: "Age", length: 3, code: 3, priority: .middle)
let cityField = inputField(header: "City", length: 15, priority: .low, placeholder: "City")

//2 задание
let customField = inputField(header: "Custom", length: 24, code: 1, priority: .high, placeholder: "Lorem ipsum")
print(customField.lengthMeasure())
print(customField.placeholder!)
customField.changePlaceholder(with: "test")
print(customField.placeholder!)

//3 задание
let customField2 = inputField(header: "Test", length: 23, priority: .high)

//4 задание
let customField3 = inputField(header: "Test2", length: 23, priority: .middle)
print(customField3.priority)

//5 задание

struct enterField{
    let header: String
    let length: Int
    var code: Int?
    var placeholder: String?
    
    func lengthMeasure()-> Bool{
        if length < 25{
            return true
        }
        return false
    }
    
    mutating func changePlaceholder(with newPlaceholder: String){
       placeholder = newPlaceholder
    }
}

var stateField = enterField(header: "State", length: 25, placeholder: "Enter the state")
print(cityField.header)
